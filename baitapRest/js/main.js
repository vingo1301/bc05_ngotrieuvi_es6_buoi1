let tinhDTB = (...danhSachDiem) => {
    console.log("danhSachDiem: ",danhSachDiem);
    let DTB = 0;
    let soMonHoc = 0;
    danhSachDiem.forEach(function(item){
        DTB += item;
        soMonHoc++;
    })
    return DTB / soMonHoc;
}
let tinhDTB1 = () => {
    let toan = document.getElementById("inpToan").value*1;
    let ly = document.getElementById("inpLy").value*1;
    let hoa = document.getElementById("inpHoa").value*1;
    let DTB1 = tinhDTB(toan,ly,hoa);
    document.getElementById("tbKhoi1").innerHTML = DTB1;
}
let tinhDTB2 = () => {
    let van = document.getElementById("inpVan").value*1;
    let su = document.getElementById("inpSu").value*1;
    let dia = document.getElementById("inpDia").value*1;
    let english = document.getElementById("inpEnglish").value*1;
    let DTB2 = tinhDTB(van,su,dia,english);
    document.getElementById("tbKhoi2").innerHTML = DTB2;
}